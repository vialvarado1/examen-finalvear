package deloitte.academy.lesson01.run;

import java.util.ArrayList;
import java.util.logging.Logger;
import deloitte.academy.lesson01.entity.Enums;
import deloitte.academy.lesson01.machine.Articulos;
import deloitte.academy.lesson01.machine.Producto;
import deloitte.academy.lesson01.machine.RegistroNuevasVentas;
import deloitte.academy.lesson01.machine.RegistroNuevosProductos;
import deloitte.academy.lesson01.machine.Ventas;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 
 * @author Victor Eduardo Alvarado Ram�rez
 * @version 1.1
 * @since 10/03/20
 *
 *
 */

/*
 * @link deloitte.academy.lesson01.run
 * @linkplain deloitte.academy.lesson01.run
 * @docRoot deloitte.academy.lesson01.run
 */

public class Run {

	private static final Logger LOGGER = Logger.getLogger(Run.class.getName());

	/**
	 * 
	 * @param args  Main
	 */
	public static void main(String[] args) {
		/*
		 * Este es el programa principal, trae y hereda de otras clases todos los datos
		 * Aqui se ponen las habitaciones disponibles como objeto
		 * 
		 * 
		 */
		@SuppressWarnings("resource")
		Scanner sn = new Scanner(System.in);
		boolean salir = false;
		String opcion; // Guardaremos la opcion del usuario
		ArrayList<Producto> listProductoA1 = new ArrayList<Producto>();
		ArrayList<Producto> listProductoA2 = new ArrayList<Producto>();
		ArrayList<Producto> listProductoA3 = new ArrayList<Producto>();

		ArrayList<Producto> listProductoA4 = new ArrayList<Producto>();
		ArrayList<Producto> listProductoA5 = new ArrayList<Producto>();
		ArrayList<Producto> listProductoA6 = new ArrayList<Producto>();

		ArrayList<Producto> listProductoB1 = new ArrayList<Producto>();
		ArrayList<Producto> listProductoB2 = new ArrayList<Producto>();
		ArrayList<Producto> listProductoB3 = new ArrayList<Producto>();

		ArrayList<Producto> listProductoB4 = new ArrayList<Producto>();
		ArrayList<Producto> listProductoB5 = new ArrayList<Producto>();
		ArrayList<Producto> listProductoB6 = new ArrayList<Producto>();

		ArrayList<Producto> listProductoC1 = new ArrayList<Producto>();
		ArrayList<Producto> listProductoC2 = new ArrayList<Producto>();
		ArrayList<Producto> listProductoC3 = new ArrayList<Producto>();
		ArrayList<Producto> listProductoC4 = new ArrayList<Producto>();
        
		

		ArrayList<Ventas> listVentas = new ArrayList<Ventas>();
		ArrayList<Ventas> listVentas2 = new ArrayList<Ventas>();
		ArrayList<Ventas> listVentas3 = new ArrayList<Ventas>();
		ArrayList<Ventas> listVentas4 = new ArrayList<Ventas>();
		ArrayList<Ventas> listVentas5 = new ArrayList<Ventas>();
		ArrayList<Ventas> listVentas6 = new ArrayList<Ventas>();
		ArrayList<Ventas> listVentas7 = new ArrayList<Ventas>();
		ArrayList<Ventas> listVentas8 = new ArrayList<Ventas>();
		ArrayList<Ventas> listVentas9 = new ArrayList<Ventas>();
		ArrayList<Ventas> listVentas10 = new ArrayList<Ventas>();
		ArrayList<Ventas> listVentas11 = new ArrayList<Ventas>();
		ArrayList<Ventas> listVentas12 = new ArrayList<Ventas>();
		ArrayList<Ventas> listVentas13 = new ArrayList<Ventas>();
		ArrayList<Ventas> listVentas14 = new ArrayList<Ventas>();
		ArrayList<Ventas> listVentas15 = new ArrayList<Ventas>();
		ArrayList<Ventas> listVentas16 = new ArrayList<Ventas>();

		Producto productoA1 = new Producto();
		Producto productoA2 = new Producto();
		Producto productoA3 = new Producto();
		Producto productoA4 = new Producto();
		Producto productoA5 = new Producto();
		Producto productoA6 = new Producto();
		Producto productoB1 = new Producto();
		Producto productoB2 = new Producto();
		Producto productoB3 = new Producto();
		Producto productoB4 = new Producto();
		Producto productoB5 = new Producto();
		Producto productoB6 = new Producto();
		Producto productoC1 = new Producto();
		Producto productoC2 = new Producto();
		Producto productoC3 = new Producto();
		Producto productoC4 = new Producto();

		Ventas a1 = new Ventas();
		Ventas a2 = new Ventas();
		Ventas a3 = new Ventas();
		Ventas a4 = new Ventas();
		Ventas a5 = new Ventas();
		Ventas a6 = new Ventas();
		Ventas b1 = new Ventas();
		Ventas b2 = new Ventas();
		Ventas b3 = new Ventas();
		Ventas b4 = new Ventas();
		Ventas b5 = new Ventas();
		Ventas b6 = new Ventas();
		Ventas c1 = new Ventas();
		Ventas c2 = new Ventas();
		Ventas c3 = new Ventas();
		Ventas c4 = new Ventas();

		/**
		 * Aqui se ponen los precios de cada producto
		 */

		Articulos COSTA1 = new Articulos(1, 10.5);
		COSTA1.setIdVenta(1);
		COSTA1.setImporte(10.50);

		Articulos COSTA2 = new Articulos(2, 15.5);
		COSTA2.setIdVenta(2);
		COSTA2.setImporte(15.5);

		Articulos COSTA3 = new Articulos(3, 22.5);
		COSTA3.setIdVenta(3);
		COSTA3.setImporte(22.5);

		Articulos COSTA4 = new Articulos(4, 8.75);
		COSTA4.setIdVenta(4);
		COSTA4.setImporte(8.75);

		Articulos COSTA5 = new Articulos(5, 30);
		COSTA5.setIdVenta(5);
		COSTA5.setImporte(30);

		Articulos COSTA6 = new Articulos(6, 15);
		COSTA6.setIdVenta(6);
		COSTA6.setImporte(15);

		Articulos COSTB1 = new Articulos(1, 10);
		COSTB1.setIdVenta(1);
		COSTB1.setImporte(10);

		Articulos COSTB2 = new Articulos(2, 120);
		COSTB2.setIdVenta(2);
		COSTB2.setImporte(120);

		Articulos COSTB3 = new Articulos(3, 10.10);
		COSTB3.setIdVenta(3);
		COSTB3.setImporte(10);

		Articulos COSTB4 = new Articulos(4, 3.14);
		COSTB4.setIdVenta(4);
		COSTB4.setImporte(3.14);

		Articulos COSTB5 = new Articulos(5, 15.55);
		COSTB5.setIdVenta(5);
		COSTB5.setImporte(15.55);

		Articulos COSTB6 = new Articulos(6, 12.25);
		COSTB6.setIdVenta(6);
		COSTB6.setImporte(12.25);

		Articulos COSTC1 = new Articulos(1, 10);
		COSTC1.setIdVenta(1);
		COSTC1.setImporte(10);

		Articulos COSTC2 = new Articulos(2, 14.75);
		COSTC2.setIdVenta(2);
		COSTC2.setImporte(14.75);

		Articulos COSTC3 = new Articulos(3, 13.15);
		COSTC3.setIdVenta(3);
		COSTC3.setImporte(13.15);

		Articulos COSTC4 = new Articulos(4, 22);
		COSTC4.setIdVenta(4);
		COSTC4.setImporte(22);

		/*
		 * Aqui se registran los produtos
		 */

		productoA1.setNombre("Chocolate");
		listProductoA1.add(productoA1);
		listProductoA1.add(productoA1);
		listProductoA1.add(productoA1);
		listProductoA1.add(productoA1);
		listProductoA1.add(productoA1);
		listProductoA1.add(productoA1);
		listProductoA1.add(productoA1);
		listProductoA1.add(productoA1);
		listProductoA1.add(productoA1);
		listProductoA1.add(productoA1);

		System.out.println("Producto:   " + productoA1.getNombre());
		System.out.println("C�digo: " + Enums.OK.descripcion + Enums.OK.num);
		System.out.println("Costo: " + COSTA1.cobrar());

		System.out.println(" ");

		int total = ((RegistroNuevosProductos.totalProducto(listProductoA1)));
		RegistroNuevosProductos registroNuevosProductosA1 = new RegistroNuevosProductos();
		listProductoA1.add(registroNuevosProductosA1.registrar(productoA1));

		if (total == 0) {
			System.out.println("No hay Chocolates disponibles");
		} else {

			System.out.println("Chocolates Disponibles  " + total);
		}
		LOGGER.info("Disponible");

		System.out.println(" ");

		productoA2.setNombre("Doritos");
		listProductoA2.add(productoA2);
		listProductoA2.add(productoA2);
		listProductoA2.add(productoA2);
		listProductoA2.add(productoA2);
		System.out.println("Producto:   " + productoA2.getNombre());
		System.out.println("C�digo: " + Enums.OK2.descripcion + Enums.OK2.num);
		System.out.println("Costo: " + COSTA2.cobrar());

		int totalA2 = (RegistroNuevosProductos.totalProducto(listProductoA2));
		RegistroNuevosProductos registroNuevosProductosA2 = new RegistroNuevosProductos();
		listProductoA2.add(registroNuevosProductosA2.registrar(productoA2));

		if (totalA2 == 0) {
			System.out.println("Ya no hay doritos disponibles");

		} else {
			System.out.println("Doritos Disponibles:  " + totalA2);
		}
		LOGGER.info("Disponible");

		System.out.println(" ");

		productoA3.setNombre("Coca");
		listProductoA3.add(productoA3);
		listProductoA3.add(productoA3);
		System.out.println("Producto " + productoA3.getNombre());
		System.out.println("C�digo: " + Enums.OK3.descripcion + Enums.OK3.num);
		System.out.println("Costo: " + COSTA3.cobrar());

		int totalA3 = (RegistroNuevosProductos.totalProducto(listProductoA3));
		RegistroNuevosProductos registroNuevosProductosA3 = new RegistroNuevosProductos();
		listProductoA3.add(registroNuevosProductosA3.registrar(productoA3));

		if (totalA3 == 0) {
			System.out.println("Ya no hay  " + productoA3.getNombre());
		} else {
			System.out.println("Coca  Disponible: " + totalA3);
		}
		LOGGER.info("Disponible");

		System.out.println(" ");

		productoA4.setNombre("Gomitas");
		listProductoA4.add(productoA4);
		listProductoA4.add(productoA4);
		listProductoA4.add(productoA4);
		listProductoA4.add(productoA4);
		listProductoA4.add(productoA4);
		listProductoA4.add(productoA4);
		System.out.println("Producto " + productoA4.getNombre());
		System.out.println("C�digo: " + Enums.OK4.descripcion + Enums.OK4.num);
		System.out.println("Costo: " + COSTA4.cobrar());

		int totalA4 = (RegistroNuevosProductos.totalProducto(listProductoA4));
		RegistroNuevosProductos registroNuevosProductosA4 = new RegistroNuevosProductos();
		listProductoA4.add(registroNuevosProductosA4.registrar(productoA4));

		if (totalA4 == 0) {
			System.out.println("Ya no hay  " + productoA4.getNombre());
		} else {
			System.out.println(productoA4.getNombre() + " Disponibles: " + totalA4);
		}
		LOGGER.info("Disponible");

		System.out.println(" ");

		productoA5.setNombre("Chips");
		listProductoA5.add(productoA5);
		listProductoA5.add(productoA5);
		listProductoA5.add(productoA5);
		listProductoA5.add(productoA5);
		listProductoA5.add(productoA5);
		listProductoA5.add(productoA5);
		listProductoA5.add(productoA5);
		listProductoA5.add(productoA5);
		listProductoA5.add(productoA5);
		listProductoA5.add(productoA5);
		System.out.println("Producto " + productoA5.getNombre());
		System.out.println("C�digo: " + Enums.OK5.descripcion + Enums.OK5.num);
		System.out.println("Costo: " + COSTA5.cobrar());

		int totalA5 = (RegistroNuevosProductos.totalProducto(listProductoA5));
		RegistroNuevosProductos registroNuevosProductosA5 = new RegistroNuevosProductos();
		listProductoA5.add(registroNuevosProductosA5.registrar(productoA5));

		if (totalA5 == 0) {
			System.out.println("Ya no hay  " + productoA5.getNombre());
		} else {
			System.out.println(productoA5.getNombre() + " Disponibles: " + totalA5);
		}
		LOGGER.info("Disponible");

		System.out.println(" ");

		productoA6.setNombre("Jugo");
		listProductoA6.add(productoA6);
		listProductoA6.add(productoA6);

		System.out.println("Producto " + productoA6.getNombre());
		System.out.println("C�digo: " + Enums.OK6.descripcion + Enums.OK6.num);
		System.out.println("Costo: " + COSTA6.cobrar());

		int totalA6 = (RegistroNuevosProductos.totalProducto(listProductoA6));
		RegistroNuevosProductos registroNuevosProductosA6 = new RegistroNuevosProductos();
		listProductoA6.add(registroNuevosProductosA6.registrar(productoA6));

		if (totalA6 == 0) {
			System.out.println("Ya no hay  " + productoA6.getNombre());
		} else {
			System.out.println(productoA6.getNombre() + " Disponibles: " + totalA6);
		}
		LOGGER.info("Disponible");

		System.out.println(" ");

		productoB1.setNombre("Galletitas");
		listProductoB1.add(productoB1);
		listProductoB1.add(productoB1);
		listProductoB1.add(productoB1);

		System.out.println("Producto " + productoB1.getNombre());
		System.out.println("C�digo: " + Enums.OK7.descripcion + Enums.OK7.num);
		System.out.println("Costo: " + COSTB1.cobrar());

		int totalB1 = (RegistroNuevosProductos.totalProducto(listProductoB1));
		RegistroNuevosProductos registroNuevosProductosB1 = new RegistroNuevosProductos();
		listProductoB1.add(registroNuevosProductosB1.registrar(productoB1));

		if (totalB1 == 0) {
			System.out.println("Ya no hay  " + productoB1.getNombre());
		} else {
			System.out.println(productoB1.getNombre() + " Disponibles: " + totalB1);
		}
		LOGGER.info("Disponible");

		System.out.println(" ");

		productoB2.setNombre("Canelitas");
		listProductoB2.add(productoB2);
		listProductoB2.add(productoB2);
		listProductoB2.add(productoB2);
		listProductoB2.add(productoB2);
		listProductoB2.add(productoB2);
		listProductoB2.add(productoB2);

		System.out.println("Producto " + productoB2.getNombre());
		System.out.println("C�digo: " + Enums.OK8.descripcion + Enums.OK8.num);
		System.out.println("Costo: " + COSTB2.cobrar());

		int totalB2 = (RegistroNuevosProductos.totalProducto(listProductoB2));
		RegistroNuevosProductos registroNuevosProductosB2 = new RegistroNuevosProductos();
		listProductoB2.add(registroNuevosProductosB2.registrar(productoB2));

		if (totalB2 == 0) {
			System.out.println("Ya no hay  " + productoB2.getNombre());
		} else {
			System.out.println(productoB2.getNombre() + " Disponibles: " + totalB2);
		}
		LOGGER.info("Disponible");

		System.out.println(" ");

		productoB3.setNombre("Halls");
		listProductoB3.add(productoB3);
		listProductoB3.add(productoB3);
		listProductoB3.add(productoB3);
		listProductoB3.add(productoB3);
		listProductoB3.add(productoB3);
		listProductoB3.add(productoB3);
		listProductoB3.add(productoB3);
		listProductoB3.add(productoB3);
		listProductoB3.add(productoB3);
		listProductoB3.add(productoB3);

		System.out.println("Producto " + productoB3.getNombre());
		System.out.println("C�digo: " + Enums.OK9.descripcion + Enums.OK9.num);
		System.out.println("Costo: " + COSTB3.cobrar());

		int totalB3 = (RegistroNuevosProductos.totalProducto(listProductoB3));
		RegistroNuevosProductos registroNuevosProductosB3 = new RegistroNuevosProductos();
		listProductoB3.add(registroNuevosProductosB3.registrar(productoB3));

		if (totalB3 == 0) {
			System.out.println("Ya no hay  " + productoB3.getNombre());
		} else {
			System.out.println(productoB3.getNombre() + " Disponibles: " + totalB3);
		}
		LOGGER.info("Disponible");

		System.out.println(" ");

		productoB4.setNombre("Tartas");
		listProductoB4.add(productoB4);
		listProductoB4.add(productoB4);
		listProductoB4.add(productoB4);
		listProductoB4.add(productoB4);
		listProductoB4.add(productoB4);
		listProductoB4.add(productoB4);
		listProductoB4.add(productoB4);
		listProductoB4.add(productoB4);
		listProductoB4.add(productoB4);
		listProductoB4.add(productoB4);

		System.out.println("Producto " + productoB4.getNombre());
		System.out.println("C�digo: " + Enums.OK10.descripcion + Enums.OK10.num);
		System.out.println("Costo: " + COSTB4.cobrar());

		int totalB4 = (RegistroNuevosProductos.totalProducto(listProductoB4));
		RegistroNuevosProductos registroNuevosProductosB4 = new RegistroNuevosProductos();
		listProductoB4.add(registroNuevosProductosB4.registrar(productoB4));

		if (totalB4 == 0) {
			System.out.println("Ya no hay  " + productoB4.getNombre());
		} else {
			System.out.println(productoB4.getNombre() + " Disponibles: " + totalB4);
		}
		LOGGER.info("Disponible");

		System.out.println(" ");

		productoB5.setNombre("Sabritas");
		listProductoB5.add(productoB5);
		listProductoB5.add(productoB5);
		listProductoB5.add(productoB5);

		System.out.println("Producto " + productoB5.getNombre());
		System.out.println("C�digo: " + Enums.OK11.descripcion + Enums.OK11.num);
		System.out.println("Costo: " + COSTB5.cobrar());

		int totalB5 = (RegistroNuevosProductos.totalProducto(listProductoB5));
		RegistroNuevosProductos registroNuevosProductosB5 = new RegistroNuevosProductos();
		listProductoB5.add(registroNuevosProductosB5.registrar(productoB5));

		if (totalB5 == 0) {
			System.out.println("Ya no hay  " + productoB5.getNombre());
		} else {
			System.out.println(productoB5.getNombre() + " Disponibles: " + totalB5);
		}
		LOGGER.info("Disponible");

		System.out.println(" ");

		productoB6.setNombre("Chetoos");
		listProductoB6.add(productoB6);
		listProductoB6.add(productoB6);
		listProductoB6.add(productoB6);
		listProductoB6.add(productoB6);
		listProductoB6.add(productoB6);
		listProductoB6.add(productoB6);

		System.out.println("Producto " + productoB6.getNombre());
		System.out.println("C�digo: " + Enums.OK12.descripcion + Enums.OK12.num);
		System.out.println("Costo: " + COSTB6.cobrar());

		int totalB6 = (RegistroNuevosProductos.totalProducto(listProductoB6));
		RegistroNuevosProductos registroNuevosProductosB6 = new RegistroNuevosProductos();
		listProductoB6.add(registroNuevosProductosB6.registrar(productoB6));

		if (totalB6 == 0) {
			System.out.println("Ya no hay  " + productoB6.getNombre());
		} else {
			System.out.println(productoB5.getNombre() + " Disponibles: " + totalB6);
		}
		LOGGER.info("Disponible");

		System.out.println(" ");

		productoC1.setNombre("Rocaleta");
		listProductoC1.add(productoC1);

		System.out.println("Producto " + productoC1.getNombre());
		System.out.println("C�digo: " + Enums.OK13.descripcion + Enums.OK13.num);
		System.out.println("Costo: " + COSTC1.cobrar());

		int totalC1 = (RegistroNuevosProductos.totalProducto(listProductoC1));
		RegistroNuevosProductos registroNuevosProductosC1 = new RegistroNuevosProductos();
		listProductoC1.add(registroNuevosProductosC1.registrar(productoC1));

		if (totalC1 == 0) {
			System.out.println("Ya no hay  " + productoC1.getNombre());
		} else {
			System.out.println(productoC1.getNombre() + " Disponibles: " + totalC1);
		}
		LOGGER.info("Disponible");

		System.out.println(" ");

		productoC2.setNombre("Rancheritos");
		listProductoC2.add(productoC2);
		listProductoC2.add(productoC2);
		listProductoC2.add(productoC2);
		listProductoC2.add(productoC2);
		listProductoC2.add(productoC2);
		listProductoC2.add(productoC2);

		System.out.println("Producto " + productoC2.getNombre());
		System.out.println("C�digo: " + Enums.OK14.descripcion + Enums.OK14.num);
		System.out.println("Costo: " + COSTC2.cobrar());

		int totalC2 = (RegistroNuevosProductos.totalProducto(listProductoC2));
		RegistroNuevosProductos registroNuevosProductosC2 = new RegistroNuevosProductos();
		listProductoC2.add(registroNuevosProductosC2.registrar(productoC2));

		if (totalC2 == 0) {
			System.out.println("Ya no hay  " + productoC2.getNombre());
		} else {
			System.out.println(productoC2.getNombre() + " Disponibles: " + totalC2);
		}
		LOGGER.info("Disponible");

		System.out.println(" ");

		productoC3.setNombre("Rufles");
		listProductoC3.add(productoC3);
		listProductoC3.add(productoC3);
		listProductoC3.add(productoC3);
		listProductoC3.add(productoC3);
		listProductoC3.add(productoC3);
		listProductoC3.add(productoC3);
		listProductoC3.add(productoC3);
		listProductoC3.add(productoC3);
		listProductoC3.add(productoC3);
		listProductoC3.add(productoC3);

		System.out.println("Producto " + productoC3.getNombre());
		System.out.println("C�digo: " + Enums.OK15.descripcion + Enums.OK15.num);
		System.out.println("Costo: " + COSTC3.cobrar());

		int totalC3 = (RegistroNuevosProductos.totalProducto(listProductoC3));
		RegistroNuevosProductos registroNuevosProductosC3 = new RegistroNuevosProductos();
		listProductoC3.add(registroNuevosProductosC3.registrar(productoC3));

		if (totalC3 == 0) {
			System.out.println("Ya no hay  " + productoC3.getNombre());
		} else {
			System.out.println(productoC3.getNombre() + " Disponibles: " + totalC3);
		}
		LOGGER.info("Disponible");

		System.out.println(" ");

		productoC4.setNombre("Pizza Fria");
		listProductoC4.add(productoC4);
		listProductoC4.add(productoC4);
		listProductoC4.add(productoC4);
		listProductoC4.add(productoC4);
		listProductoC4.add(productoC4);
		listProductoC4.add(productoC4);
		listProductoC4.add(productoC4);
		listProductoC4.add(productoC4);
		listProductoC4.add(productoC4);
		listProductoC4.add(productoC4);

		System.out.println("Producto " + productoC4.getNombre());
		System.out.println("C�digo: " + Enums.OK16.descripcion + Enums.OK16.num);
		System.out.println("Costo: " + COSTC4.cobrar());

		int totalC4 = (RegistroNuevosProductos.totalProducto(listProductoC4));
		RegistroNuevosProductos registroNuevosProductosC4 = new RegistroNuevosProductos();
		listProductoC4.add(registroNuevosProductosC4.registrar(productoC4));

		if (totalC4 == 0) {
			System.out.println("Ya no hay  " + productoC4.getNombre());
		} else {
			System.out.println(productoC4.getNombre() + " Disponibles: " + totalC4);
		}
		LOGGER.info("Disponible");

		/**
		 * Aqui inicia el switch
		 * 
		 * 
		 * 
		 */

		while (!salir) {

			try {

				System.out.println("Inserta C�digo");
				opcion = sn.nextLine();

				switch (opcion) {
				case "A1":

					int totala1 = total - (RegistroNuevasVentas.totalProductoVentas(listVentas));

					if (totala1 >= 1) {

						int totalVentas = (RegistroNuevasVentas.totalProductoVentas(listVentas));

						RegistroNuevasVentas registroNuevasVentas = new RegistroNuevasVentas();
						listVentas.add(registroNuevasVentas.registrarVentas(a1));

						if (totalVentas == 0) {
							System.out.println("Esta por comprar: " + productoA1.getNombre());
							System.out.println("Presionar el codigo de nuevo si desea comprarlo");

						} else {
							System.out.println("Usted a comprado: " + productoA1.getNombre());
							System.out.println("Total de Ventas de " + productoA1.getNombre() + ": " + totalVentas);
						}
					}

					else {
						System.out.println("No hay suficiente producto");
					}
					break;
				case "A2":
					int totala2 = totalA2 - (RegistroNuevasVentas.totalProductoVentas(listVentas2));
					if (totala2 >= 1) {

						int totalVentas2 = (RegistroNuevasVentas.totalProductoVentas(listVentas2));
						RegistroNuevasVentas registroNuevasVentas = new RegistroNuevasVentas();
						listVentas2.add(registroNuevasVentas.registrarVentas(a2));
						if (totalVentas2 == 0) {
							System.out.println("Esta por comprar: " + productoA2.getNombre());
							System.out.println("Presionar el codigo de nuevo si desea comprarlo");

						} else {
							System.out.println("Usted a comprado: " + productoA2.getNombre());
							System.out.println("Total de Ventas de " + productoA2.getNombre() + ": " + totalVentas2);
						}
					} else {
						System.out.println("No hay suficiente producto");
					}
					break;
				case "A3":
					int totala3 = totalA3 - (RegistroNuevasVentas.totalProductoVentas(listVentas3));
					if (totala3 >= 1) {

						int totalVentas3 = (RegistroNuevasVentas.totalProductoVentas(listVentas3));
						RegistroNuevasVentas registroNuevasVentas = new RegistroNuevasVentas();
						listVentas3.add(registroNuevasVentas.registrarVentas(a3));
						if (totalVentas3 == 0) {
							System.out.println("Esta por comprar: " + productoA3.getNombre());
							System.out.println("Presionar el codigo de nuevo si desea comprarlo");

						} else {
							System.out.println("Usted a comprado: " + productoA3.getNombre());
							System.out.println("Total de Ventas de " + productoA3.getNombre() + ": " + totalVentas3);
						}
					} else {
						System.out.println("No hay suficiente producto");
					}
					break;
				case "A4":
					int totala4 = totalA4 - (RegistroNuevasVentas.totalProductoVentas(listVentas4));
					if (totala4 >= 1) {

						int totalVentas4 = (RegistroNuevasVentas.totalProductoVentas(listVentas4));
						RegistroNuevasVentas registroNuevasVentas = new RegistroNuevasVentas();
						listVentas4.add(registroNuevasVentas.registrarVentas(a4));
						if (totalVentas4 == 0) {
							System.out.println("Esta por comprar: " + productoA4.getNombre());
							System.out.println("Presionar el codigo de nuevo si desea comprarlo");

						} else {
							System.out.println("Usted a comprado: " + productoA4.getNombre());
							System.out.println("Total de Ventas de " + productoA4.getNombre() + ": " + totalVentas4);
						}
					} else {
						System.out.println("No hay suficiente producto");
					}
					break;
				case "A5":
					int totala5 = totalA5 - (RegistroNuevasVentas.totalProductoVentas(listVentas5));
					if (totala5 >= 1) {

						int totalVentas5 = (RegistroNuevasVentas.totalProductoVentas(listVentas5));
						RegistroNuevasVentas registroNuevasVentas = new RegistroNuevasVentas();
						listVentas5.add(registroNuevasVentas.registrarVentas(a5));
						if (totalVentas5 == 0) {
							System.out.println("Esta por comprar: " + productoA5.getNombre());
							System.out.println("Presionar el codigo de nuevo si desea comprarlo");

						} else {
							System.out.println("Usted a comprado: " + productoA5.getNombre());
							System.out.println("Total de Ventas de " + productoA5.getNombre() + ": " + totalVentas5);
						}
					} else {
						System.out.println("No hay suficiente producto");
					}
					break;
				case "A6":
					int totala6 = totalA6 - (RegistroNuevasVentas.totalProductoVentas(listVentas6));
					if (totala6 >= 1) {

						int totalVentas6 = (RegistroNuevasVentas.totalProductoVentas(listVentas6));
						RegistroNuevasVentas registroNuevasVentas = new RegistroNuevasVentas();
						listVentas6.add(registroNuevasVentas.registrarVentas(a6));
						if (totalVentas6 == 0) {
							System.out.println("Esta por comprar: " + productoA6.getNombre());
							System.out.println("Presionar el codigo de nuevo si desea comprarlo");

						} else {
							System.out.println("Usted a comprado: " + productoA6.getNombre());
							System.out.println("Total de Ventas de " + productoA6.getNombre() + ": " + totalVentas6);
						}
					} else {
						System.out.println("No hay suficiente producto");
					}
					break;
				case "B1":
					int totala7 = totalB1 - (RegistroNuevasVentas.totalProductoVentas(listVentas7));
					if (totala7 >= 1) {

						int totalVentas7 = (RegistroNuevasVentas.totalProductoVentas(listVentas7));
						RegistroNuevasVentas registroNuevasVentas = new RegistroNuevasVentas();
						listVentas7.add(registroNuevasVentas.registrarVentas(b1));
						if (totalVentas7 == 0) {
							System.out.println("Esta por comprar: " + productoB1.getNombre());
							System.out.println("Presionar el codigo de nuevo si desea comprarlo");

						} else {
							System.out.println("Usted a comprado: " + productoB1.getNombre());
							System.out.println("Total de Ventas de " + productoB1.getNombre() + ": " + totalVentas7);
						}
					} else {
						System.out.println("No hay suficiente producto");
					}
					break;
				case "B2":
					int totala8 = totalB2 - (RegistroNuevasVentas.totalProductoVentas(listVentas8));
					if (totala8 >= 1) {

						int totalVentas8 = (RegistroNuevasVentas.totalProductoVentas(listVentas8));
						RegistroNuevasVentas registroNuevasVentas = new RegistroNuevasVentas();
						listVentas8.add(registroNuevasVentas.registrarVentas(b2));
						if (totalVentas8 == 0) {
							System.out.println("Esta por comprar: " + productoB2.getNombre());
							System.out.println("Presionar el codigo de nuevo si desea comprarlo");

						} else {
							System.out.println("Usted a comprado: " + productoB2.getNombre());
							System.out.println("Total de Ventas de " + productoB2.getNombre() + ": " + totalVentas8);
						}
					} else {
						System.out.println("No hay suficiente producto");
					}
					break;
				case "B3":
					int totala9 = totalB3 - (RegistroNuevasVentas.totalProductoVentas(listVentas9));
					if (totala9 >= 1) {

						int totalVentas9 = (RegistroNuevasVentas.totalProductoVentas(listVentas9));
						RegistroNuevasVentas registroNuevasVentas = new RegistroNuevasVentas();
						listVentas9.add(registroNuevasVentas.registrarVentas(b3));
						if (totalVentas9 == 0) {
							System.out.println("Esta por comprar: " + productoB3.getNombre());
							System.out.println("Presionar el codigo de nuevo si desea comprarlo");

						} else {
							System.out.println("Usted a comprado: " + productoB3.getNombre());
							System.out.println("Total de Ventas de " + productoB3.getNombre() + ": " + totalVentas9);
						}
					} else {
						System.out.println("No hay suficiente producto");
					}
					break;
				case "B4":
					int totala10 = totalB4 - (RegistroNuevasVentas.totalProductoVentas(listVentas10));
					if (totala10 >= 1) {

						int totalVentas10 = (RegistroNuevasVentas.totalProductoVentas(listVentas10));
						RegistroNuevasVentas registroNuevasVentas = new RegistroNuevasVentas();
						listVentas10.add(registroNuevasVentas.registrarVentas(b4));
						if (totalVentas10 == 0) {
							System.out.println("Esta por comprar: " + productoB4.getNombre());
							System.out.println("Presionar el codigo de nuevo si desea comprarlo");

						} else {
							System.out.println("Usted a comprado: " + productoB4.getNombre());
							System.out.println("Total de Ventas de " + productoB4.getNombre() + ": " + totalVentas10);
						}
					} else {
						System.out.println("No hay suficiente producto");
					}
					break;
				case "B5":
					int totala11 = totalB5 - (RegistroNuevasVentas.totalProductoVentas(listVentas11));
					if (totala11 >= 1) {

						int totalVentas11 = (RegistroNuevasVentas.totalProductoVentas(listVentas11));
						RegistroNuevasVentas registroNuevasVentas = new RegistroNuevasVentas();
						listVentas11.add(registroNuevasVentas.registrarVentas(b5));
						if (totalVentas11 == 0) {
							System.out.println("Esta por comprar: " + productoB5.getNombre());
							System.out.println("Presionar el codigo de nuevo si desea comprarlo");

						} else {
							System.out.println("Usted a comprado: " + productoB5.getNombre());
							System.out.println("Total de Ventas de " + productoB5.getNombre() + ": " + totalVentas11);
						}
					} else {
						System.out.println("No hay suficiente producto");
					}
					break;

				case "B6":
					int totala12 = totalB6 - (RegistroNuevasVentas.totalProductoVentas(listVentas12));
					if (totala12 >= 1) {

						int totalVentas12 = (RegistroNuevasVentas.totalProductoVentas(listVentas12));
						RegistroNuevasVentas registroNuevasVentas = new RegistroNuevasVentas();
						listVentas12.add(registroNuevasVentas.registrarVentas(b6));
						if (totalVentas12 == 0) {
							System.out.println("Esta por comprar: " + productoB6.getNombre());
							System.out.println("Presionar el codigo de nuevo si desea comprarlo");

						} else {
							System.out.println("Usted a comprado: " + productoB6.getNombre());
							System.out.println("Total de Ventas de " + productoB6.getNombre() + ": " + totalVentas12);
						}
					} else {
						System.out.println("No hay suficiente producto");
					}
					break;
				case "C1":
					int totala13 = totalC1 - (RegistroNuevasVentas.totalProductoVentas(listVentas13));
					if (totala13 >= 1) {

						int totalVentas13 = (RegistroNuevasVentas.totalProductoVentas(listVentas13));
						RegistroNuevasVentas registroNuevasVentas = new RegistroNuevasVentas();
						listVentas13.add(registroNuevasVentas.registrarVentas(c1));
						if (totalVentas13 == 0) {
							System.out.println("Esta por comprar: " + productoC1.getNombre());
							System.out.println("Presionar el codigo de nuevo si desea comprarlo");

						} else {
							System.out.println("Usted a comprado: " + productoC1.getNombre());
							System.out.println("Total de Ventas de " + productoC1.getNombre() + ": " + totalVentas13);
						}
					} else {
						System.out.println("No hay suficiente producto");
					}
					break;
				case "C2":
					int totala14 = totalC2 - (RegistroNuevasVentas.totalProductoVentas(listVentas14));
					if (totala14 >= 1) {

						int totalVentas14 = (RegistroNuevasVentas.totalProductoVentas(listVentas14));
						RegistroNuevasVentas registroNuevasVentas = new RegistroNuevasVentas();
						listVentas14.add(registroNuevasVentas.registrarVentas(c2));
						if (totalVentas14 == 0) {
							System.out.println("Esta por comprar: " + productoC2.getNombre());
							System.out.println("Presionar el codigo de nuevo si desea comprarlo");

						} else {
							System.out.println("Usted a comprado: " + productoC2.getNombre());
							System.out.println("Total de Ventas de " + productoC2.getNombre() + ": " + totalVentas14);
						}
					} else {
						System.out.println("No hay suficiente producto");
					}
					break;
				case "C3":
					int totala15 = totalC3 - (RegistroNuevasVentas.totalProductoVentas(listVentas15));
					if (totala15 >= 1) {

						int totalVentas15 = (RegistroNuevasVentas.totalProductoVentas(listVentas15));
						RegistroNuevasVentas registroNuevasVentas = new RegistroNuevasVentas();
						listVentas15.add(registroNuevasVentas.registrarVentas(c3));
						if (totalVentas15 == 0) {
							System.out.println("Esta por comprar: " + productoC3.getNombre());
							System.out.println("Presionar el codigo de nuevo si desea comprarlo");

						} else {
							System.out.println("Usted a comprado: " + productoC3.getNombre());
							System.out.println("Total de Ventas de " + productoC3.getNombre() + ": " + totalVentas15);
						}
					} else {
						System.out.println("No hay suficiente producto");
					}
					break;

				case "C4":
					int totala16 = totalC3 - (RegistroNuevasVentas.totalProductoVentas(listVentas16));
					if (totala16 >= 1) {

						int totalVentas16 = (RegistroNuevasVentas.totalProductoVentas(listVentas16));
						RegistroNuevasVentas registroNuevasVentas = new RegistroNuevasVentas();
						listVentas16.add(registroNuevasVentas.registrarVentas(c4));
						if (totalVentas16 == 0) {
							System.out.println("Esta por comprar: " + productoC4.getNombre());
							System.out.println("Presionar el codigo de nuevo si desea comprarlo");

						} else {
							System.out.println("Usted a comprado: " + productoC4.getNombre());
							System.out.println("Total de Ventas de " + productoC4.getNombre() + ": " + totalVentas16);
						}
					} else {
						System.out.println("No hay suficiente producto");
					}
					break;

				case "Salir":
					salir = true;
					break;
				default:
					System.out.println("Insertar un codigo valido");
				}
			}
			
			catch (InputMismatchException e) {
				System.out.println("Debes insertar un codigo valido");
				sn.nextLine();
			}
		}

	}


}
