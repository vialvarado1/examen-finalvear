package deloitte.academy.lesson01.entity;
/**
 * 
 * @author Victor Alvarado
 * @version 1.1
 * @since 10/03/20
 *
 */
public enum Enums {
	
	OK("A",1),OK2("A",2),OK3("A",3),OK4("A",4),OK5("A",5),OK6("A",6),OK7("B",1),OK8("B",2),OK9("B",3),OK10("B",4),OK11("B",5),OK12("B",6),OK13("C",1),OK14("C",2),OK15("C",3),OK16("C",4);
/**
	{@code <code>&</code> Enums funciona para guardar valores del tipo string e int}	
	*/
	
	/**
	 * 
	 * {@code  String descripcion sirve para generar la descripcion de cada articulo <code>@Foo</code>; and a generic List<String>}.
	 */
public String descripcion;
public int num;
/**
 * 
 * @param descripcion es la descripcion de nuestro enum
 * @param num es el numero del porcentaje de descuento
 */


private Enums(String descripcion, int num) {
	this.descripcion = descripcion;
	this.num = num;
	/**
	 * Se generan getters and setters
	 */
}

public String getDescripcion() {
	return descripcion;
}

public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}

public int getNum() {
	return num;
}

public void setNum(int num) {
	this.num = num;
}



}