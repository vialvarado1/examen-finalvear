package deloitte.academy.lesson01.entity;
/**
 * 
 * @author Victor Eduardo Alvarado Ram�rez
 * @since 10/03/20
 * @version 1.1
 *
 */

public abstract class Abstractos {
//Sirve para ayudar a otras clases para funcionar pero en si no hace nada por si misma
	
	private int idVenta;
	private double importe;


	
	/**
	 * 
	 * @param idVenta es el ID de cada tipo de producto y es del tipo entero
	 * @param importe es el costo y es del tipo double
	 */

	public Abstractos(int idVenta, double importe) {
		super();
		this.idVenta = idVenta;
		this.importe = importe;
	}

/**
 * Genera gets a sets
 * @return retorna los datos del metodo
 */

	public int getIdVenta() {
		return idVenta;
	}

	public void setIdVenta(int idVenta) {
		this.idVenta = idVenta;
	}

	public double getImporte() {
		return importe;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}
	public abstract  double cobrar();
	
}
