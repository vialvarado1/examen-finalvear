package deloitte.academy.lesson01.machine;

import java.util.ArrayList;

/**
 * 
 * @author Victor Alvarado
 * @since 11/03/20
 * @version 1.1
 *         Este programa cuenta el numero de habitaciones para usarlo en run
 *
 */

public class RegistroNuevosProductos {

	public static int x;
	/**
	 * 
	 * @param productoNuevo establece un nuevo producto
	 * @return retorna un valor
	 */

	public Producto registrar(Producto productoNuevo) {
		/**
		 * El objeto persona nos permite obtener los nombre y nos retorna el nombre
		 */
		Producto objPersona = new Producto();
		objPersona.setNombre(productoNuevo.getNombre());

		return productoNuevo;

	}

	/**
	 * 
	 * @param producto producto es el registro de la habitacion
	 * @return nos retorna el total de habitaciones disponibles
	 */
	public static int totalProducto(ArrayList<Producto> producto) {
		int total = 0;
		total = producto.size();
		return total;
	}
}
