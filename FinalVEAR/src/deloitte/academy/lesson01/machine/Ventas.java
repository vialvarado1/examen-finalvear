package deloitte.academy.lesson01.machine;

import deloitte.academy.lesson01.entity.Abstractos;

/**
 * 
 * @author vialvarado
 * @version 1.1
 * @since 10/03/20
 *
 */

public class Ventas extends Abstractos {
	/**
	 * Esta clase genera los descuentos para las memorias
	 * 
	 */
	@Override
	public double cobrar() {
		/**
		 * total es el total del descuento al 10%
		 */
		
		

		double total = this.getImporte();
		return total;
	}

	/**
	 * El metodo memorias tiene dos parametros uno id Venta u otro de importe
	 */
	public Ventas() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @param idVenta: este parametro es para identificar el tipo de venta
	 * @param importe: este es el costo de la habitacion
	 */
	public Ventas(int idVenta, double importe) {
		super(idVenta, importe);
		// TODO Auto-generated constructor stub
	}

	public Object getNombre() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setNombre(Object nombre) {
		// TODO Auto-generated method stub
		
	}

}
