package deloitte.academy.lesson01.machine;

import deloitte.academy.lesson01.entity.Abstractos;
/**
 * 
 * @author Victor Alvarado
 * @version 1.1
 * @since 10/03/20
 *
 */

/**
 * Sirve para mandar a llamar los articulos para despues utilizarlos en el run
 *
 *@see #Articulos(int, double)
 *
 */

public class Articulos extends Abstractos {
	/**
	 * Este programa registra y genera el porcentaje de descuento y lo envia a Run
	 */
	
	public Articulos() {
		super();
		// TODO Auto-generated constructor stub
	}
/**
 * 
 * @param idVenta El ide del tipo entero del producto
 * @param importe El costo total, y es un entero
 */
	public Articulos(int idVenta, double importe) {
		super (idVenta, importe);
		// TODO Auto-generated constructor stub
	}

	public double cobrar ()	{
		double total = this.getImporte();
		return total;
	/**
	 * @return Return total retorna un entero con el tama�o de las listas
	 */
	
}
}
