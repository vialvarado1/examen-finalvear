package deloitte.academy.lesson01.machine;

/**
 * Este programa guarda el nombre para usarlo run
 */
import java.util.ArrayList;
/**
 * 
 * @author Victor Eduardo Alvarado Ram�rez
 * @version 1.1
 * @since 11/03/20
 *
 */
public class Producto {
/**
 * Se declaran las variables 
 */
	public String nombre;
	public int ide ;
	public int cantidad;
	
	
public Producto() {
	super();
}

/**
 * @see #cantidad
 * @return retorna el nombre
 */

public String getNombre()

{
	return nombre;
}

/**
 * 
 * @param nombre de los huespedes del tipo string
 */
public void setNombre(String nombre) {
	this.nombre = nombre;
}

/**
 * 
 * @return retorna el nombre como tipo String
 */
public int getIde() {
return ide;
}
/**
 * 
 * @param ide del tipo entero es un identificador/codigo para cada producto
 */
public void setIde(int ide) {
this.ide = ide;
}
/**
 * 
 * @return retorna la cantidad 
 */
public int getCantidad() {
return cantidad;
}
/**
 * 
 * @param cantidad Establece una cantidad para despues contabilizarla y es del tipo entero
 */
public void setCantidad(int cantidad) {
this.cantidad = cantidad;
}
/**
 * 
 * @param nombre guarda en un String el nombre 
 * @param ide guarda en un int un numero o codigo identificador
 * @param cantidad establece las cantidades
 * @see #totalProductoLac(ArrayList)
 */


public Producto(String nombre, int ide, int cantidad) {
	/**
	 *  Se declaran las variables 
	 */
	super();
	this.nombre = nombre;
	this.ide = ide;
	this.cantidad = cantidad;
}

/**
 * 
 * @param valor1 del tipo entero
 * @param valor2 del tipo entero
 * @return retorna el valor de la suma como resulado
 */
public static int sumar(int valor1, int valor2) {
	int resultado = 0;
	resultado = valor1+ valor2;
	return resultado;}
	/**
	 * 
	 * @param producto del tipo registrar
	 * @return retorna el objeto
	 */

public Producto registar(Producto producto)
	{

		Producto objProducto = new Producto();
		objProducto.setNombre(producto.getNombre());
		
		return objProducto;
	}
/**
 * 
 * @param ListaProducto Es una lista donde guardar los objetos de los productos
 * @return retorna el total
 */
 public static int totalProductoLac( ArrayList<Producto> ListaProducto) {
	 int Total=0;
	
	 return Total;
 }
}

     


